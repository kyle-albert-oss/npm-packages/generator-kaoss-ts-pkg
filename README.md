# generator-kaoss-ts-pkg

Generator for TS npm packages.

## Development

### Testing the generator

#### Prerequisites

- `yo` installed (`npm i -g yo`)
- `yeoman-environment` installed (`npm i -g yeoman-environment`)

#### Workflow

1. Make changes to code.

2. From this project's directory:

   ```shell script
   npm link
   cd /path/to/new/project
   yo kaoss-ts-pkg
   ```

   Alternatively for yeoman-environment (temporary?): `yoe run kaoss-ts-pkg`

## Resources

- [Yeoman Docs](https://yeoman.io/authoring/)
- [Embedded JavaScript templating (EJS)](https://ejs.co/) for file templating docs.
- [Existing generator search](https://yeoman.io/generators/)

### Issues

- [TypeError: this.npmInstall is not a function](https://github.com/yeoman/generator/issues/1283)
- [Generator broken after updating to 5.0.0](https://github.com/yeoman/generator/issues/1279)

## License

MIT

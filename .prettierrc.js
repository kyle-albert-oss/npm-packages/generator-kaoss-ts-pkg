/**
 * https://prettier.io/docs/en/options.html
 */
const prettierConfig = {
	arrowParens: "always",
	bracketSpacing: true,
	// endOfLine: "lf", // comes from editorconfig
	htmlWhitespaceSensitivity: "css",
	insertPragma: false,
	jsxBracketSameLine: false,
	jsxSingleQuote: false,
	quoteProps: "as-needed",
	printWidth: 140,
	proseWrap: "preserve",
	requirePragma: false,
	semi: true,
	singleQuote: false,
	// useTabs: true, // comes from editorconfig
	trailingComma: "es5",
};

module.exports = prettierConfig;

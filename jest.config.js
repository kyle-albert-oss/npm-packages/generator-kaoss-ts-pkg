/* eslint-disable @typescript-eslint/no-var-requires */
/**
 * https://jestjs.io/docs/en/configuration
 */
module.exports = {
	globals: {
		"ts-jest": {
			tsConfig: "tsconfig.test.json",
		},
	},
	preset: "ts-jest",
	testEnvironment: "node",
	moduleFileExtensions: ["js", "jsx", "ts", "tsx"],
	modulePathIgnorePatterns: ["<rootDir>/coverage/", "<rootDir>/dist/", "<rootDir>/out/", "<rootDir>/tmp/"],
	//	setupFilesAfterEnv: ["./tests/setup-tests.ts"],
	transform: {
		"\\.(tsx?)$": "ts-jest",
	},
};

import * as path from "path";
import { nodePkgs } from "../package-versions";
import { GENERATOR_NODE_MODULES_PATH, ExtendedGenerator } from "../util";

export default class extends ExtendedGenerator {
	writing(): void {
		this.fs.copy(
			path.join(GENERATOR_NODE_MODULES_PATH, "cra-template-typescript", "template", "public"),
			this.destinationPath("client", "public")
		);
		this.fs.copy(
			path.join(GENERATOR_NODE_MODULES_PATH, "cra-template-typescript", "template", "src"),
			this.destinationPath("client", "src")
		);
	}

	install(): void {
		const baseOpts = {
			cwd: this.destinationPath("client"),
			tool: "npm",
		} as const;
		this.nodeModuleInstall({
			...baseOpts,
			pkgs: nodePkgs(["react", "react-dom", "react-scripts", "web-vitals"]),
		});
		this.nodeModuleInstall({
			...baseOpts,
			pkgs: nodePkgs([
				"@testing-library/jest-dom", // TODO move to jest generator?
				"@testing-library/react",
				"@testing-library/user-event",
				"@types/react",
				"@types/react-dom",
			]),
			dev: true,
		});
	}
}

/**
 * Methods are ordered by priority.
 * https://yeoman.io/authoring/running-context.html
 */
export interface TStandardGeneratorLifecycle {
	initializing?(): void;
	prompting?(): Promise<void>;
	configuring?(): void;
	// default priority for non-matching method name here
	writing?(): void;
	conflicts?(): void;
	install?(): void;
	end?(): void;
}

export enum PackagePlatform {
	BROWSER = "Browser",
	NODE = "Node",
	WEB_APP = "Web App",
}

export enum NodePackageFunction {
	CLI = "CLI",
	DEPENDENCY = "Dependency",
	EXPRESS_SERVER = "Express Server",
}

export enum WebAppFunction {
	CLIENT = "Client",
	SERVER = "Server",
}

export enum WebAppClient {
	REACT = "React",
}

export enum CiTool {
	GITLAB = "GitLab CI",
}

export interface TAnswers {
	ciTool: string;
	gitDomain: string;
	gitRepoPath: string;
	gitRepoName: string;
	nodeVersion: string;
	packageAuthor: string;
	packageDescription: string;
	packageFunction?: NodePackageFunction;
	packageName: string;
	packagePlatform: PackagePlatform;
	packageScope?: string;
}

export interface TComputedAnswers {
	fullPackageName: string;
	nodeMajorVersion: number;
}

export interface TAllAnswers extends TAnswers, TComputedAnswers {}

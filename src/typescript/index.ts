import { PackagePlatform, WebAppFunction, NodePackageFunction } from "../types";
import { nodePkgs } from "../package-versions";
import { ExtendedGenerator } from "../util";

export default class extends ExtendedGenerator {
	tsModule?: string;
	tsTarget?: string;
	tsLibs?: string[];

	configuring(): void {
		// TODO verify the module/target stuff more
		const packagePlatform = this.configGet("packagePlatform");
		const packageFunction = this.configGet("packageFunction");
		const { webAppFunction, webAppClient } = this.options;
		const isBrowserTargeted = webAppFunction === WebAppFunction.CLIENT || packagePlatform === PackagePlatform.BROWSER;
		const isNodeDependency = packagePlatform === PackagePlatform.NODE && packageFunction === NodePackageFunction.DEPENDENCY;
		this.tsModule = isBrowserTargeted || isNodeDependency ? "ESNext" : "CommonJS"; // Node expects commonjs however rollup expects esnext

		this.tsLibs = [];
		if (webAppClient) {
			this.tsTarget = "ES5";
			this.tsLibs = ["dom", "dom.iterable", "ESNext"];
		} else {
			this.tsTarget = "ES2020"; // TODO this should depend on node version for server stuff
			this.tsLibs = ["dom", "dom.iterable", "ES2020"];
		}
	}

	writing(): void {
		const { webAppFunction } = this.options;
		this.fs.copyTpl(
			this.templatePath("tsconfig.json.ejs"),
			this.destinationPath(webAppFunction === WebAppFunction.CLIENT ? "client/tsconfig.json" : "tsconfig.json"),
			{
				...this.options,
				tsModule: this.tsModule,
				tsTarget: this.tsTarget,
				tsLibs: this.tsLibs ?? [],
			}
		);
		this.fs.copy(
			this.templatePath("tsconfig.eslint.json.ejs"),
			this.destinationPath(webAppFunction === WebAppFunction.CLIENT ? "client/tsconfig.eslint.json" : "tsconfig.eslint.json")
		);
	}

	install(): void {
		this.nodeModuleInstall({ tool: "npm", pkgs: nodePkgs(["ts-essentials", "ts-node", "tsdx", "tslib", "typescript"]), dev: true });
	}
}

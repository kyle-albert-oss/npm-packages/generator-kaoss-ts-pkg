import { ExtendedGenerator } from "../util";

export default class extends ExtendedGenerator {
	writing(): void {
		this.fs.copyTpl(this.templatePath("nvmrc.ejs"), this.destinationPath("./.nvmrc"), {
			nodeVersion: this.configGet("nodeVersion") || process.versions.node,
		});
	}
}

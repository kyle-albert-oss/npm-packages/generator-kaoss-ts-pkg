import { ExtendedGenerator } from "../util";

export default class extends ExtendedGenerator {
	writing(): void {
		this.fs.copyTpl(this.templatePath("readme.md.ejs"), this.destinationPath("README.md"), {
			fullPackageName: this.configGet("fullPackageName"),
			nodeVersion: this.configGet("nodeVersion"),
			packageDescription: this.configGet("packageDescription"),
			packageName: this.configGet("packageName"),
		});
	}
}

export const PACKAGE_VERSIONS: { [key: string]: string } = {
	// value should be syntax supported by version in: npm i pkg@version
	"@types/jest": "26",
	husky: "4", // licensing
	jest: "26.6.0", // because of create-react-app
	typescript: "4.2",
};

export const nodePkgs = (pkgs: string[]): string[] => pkgs.map((p) => (PACKAGE_VERSIONS[p] ? `${p}@${PACKAGE_VERSIONS[p]}` : p));

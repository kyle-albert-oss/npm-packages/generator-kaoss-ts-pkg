import Generator from "yeoman-generator";
import { NodePackageFunction, PackagePlatform, CiTool, TAnswers, WebAppFunction, WebAppClient } from "../types";
import { ExtendedGenerator } from "../util";

export default class extends ExtendedGenerator {
	answers?: TAnswers;

	async prompting(): Promise<void> {
		this.answers = await this.prompt(this._packageInitQuestions());

		Object.assign(
			this.answers,
			await this.prompt([...this._packageMetaQuestions(this.answers.packageScope), ...this._vcsQuestions(), ...this._ciQuestions()])
		);

		this.answers.packageFunction = this.answers.packageFunction || NodePackageFunction.DEPENDENCY;

		const getComposeOptions = () => ({
			...this.options,
			answers: {
				...this.answers,
			},
		});

		if (this.answers.packageFunction === NodePackageFunction.EXPRESS_SERVER) {
			// TODO: future node versions will support ESM out-of-box
			this.composeWith(require.resolve("../express"), getComposeOptions());
		} else if (this.answers.packageFunction === NodePackageFunction.CLI) {
			this.composeWith(require.resolve("../cli"), getComposeOptions());
		}

		const webAppClient = WebAppClient.REACT;

		// begin always-project-root directory generators
		this.composeWith(require.resolve("../package"), {
			...getComposeOptions(),
			...(this.answers.packagePlatform === PackagePlatform.WEB_APP ? { webAppClient, webAppFunction: WebAppFunction.SERVER } : {}),
		});
		this.composeWith(require.resolve("../editorconfig"), getComposeOptions());
		this.composeWith(require.resolve("../eslint"), getComposeOptions());
		this.composeWith(require.resolve("../gitignore"), getComposeOptions());
		this.composeWith(require.resolve("../jest"), getComposeOptions());
		this.composeWith(require.resolve("../nvm"), getComposeOptions());
		this.composeWith(require.resolve("../prettier"), getComposeOptions());
		this.composeWith(require.resolve("../readme"), getComposeOptions());
		this.composeWith(require.resolve("../typescript"), getComposeOptions());
		if (this.answers.ciTool === CiTool.GITLAB) {
			this.composeWith(require.resolve("../gitlab"), getComposeOptions());
		}
		if (this.answers.packagePlatform === PackagePlatform.WEB_APP) {
			this.composeWith(require.resolve("../express"), {
				...getComposeOptions(),
				indexRootSubDir: "server",
				webAppClient,
				webAppFunction: WebAppFunction.SERVER,
			});
		}
		// end always-project-root directory generators

		// begin web-app client subdirectory generators
		if (this.answers.packagePlatform === PackagePlatform.WEB_APP) {
			const clientComposeOptions = {
				...getComposeOptions(),
				//destinationRoot: clientPath, // doesn't seem to have effect
				webAppClient,
				webAppFunction: WebAppFunction.CLIENT,
			};
			this.composeWith(require.resolve("../package"), clientComposeOptions);
			this.composeWith(require.resolve("../jest"), clientComposeOptions);
			this.composeWith(require.resolve("../eslint"), clientComposeOptions);
			this.composeWith(require.resolve("../typescript"), clientComposeOptions);
			this.composeWith(require.resolve("../react"), clientComposeOptions);
			//this.composeWith(require.resolve("../storybook"), clientComposeOptions); // TODO
		}
		// end web-app client subdirectory generators
	}

	private _packageInitQuestions(): Generator.Question<TAnswers>[] {
		return [
			{
				type: "list",
				name: "packagePlatform",
				message: "Package target platform?",
				choices: [PackagePlatform.NODE, PackagePlatform.BROWSER, PackagePlatform.WEB_APP],
				default: PackagePlatform.NODE,
			},
			{
				type: "list",
				name: "packageFunction",
				message: "Package function (will be used as...)?",
				when: (currentAnswers: any) => currentAnswers.packagePlatform === PackagePlatform.NODE,
				choices: [NodePackageFunction.DEPENDENCY, NodePackageFunction.CLI, NodePackageFunction.EXPRESS_SERVER],
				default: NodePackageFunction.DEPENDENCY,
			},
			{
				type: "input",
				name: "nodeVersion",
				message: "[Node] Node semver version (ex: 14.15.4)?",
				default: process.versions.node,
			},
			{
				type: "input",
				name: "packageScope",
				message: "[pkg] Package scope (ex: @my-scope)?",
				validate: (input) => (input && !input.startsWith("@") ? "Must start with @" : true),
			},
		];
	}

	private _packageMetaQuestions(packageScope: string | undefined): Generator.Question<TAnswers>[] {
		return [
			{
				type: "input",
				name: "packageName",
				message: "[pkg] Package name?",
				suffix: packageScope && packageScope !== "None" ? ` ${packageScope}/` : undefined,
				validate(input: string) {
					if (!input) {
						return "Package name is required.";
					}
					return true;
				},
			},
			{
				type: "input",
				name: "packageDescription",
				message: "[pkg] Package description?",
				prefix: "(Optional)",
			},
			{
				type: "input",
				name: "packageAuthor",
				message: "[pkg] Package author?",
				default: this.user.git.name(),
				store: true,
			},
		];
	}

	private _vcsQuestions(): Generator.Question<TAnswers>[] {
		return [
			{
				type: "input",
				name: "gitDomain",
				message: "[Git] Git domain (ex: github.com, gitlab.com)?",
				store: true,
			},
			{
				type: "input",
				name: "gitRepoPath",
				message: "[Git] Repo path (excluding repo itself, ex: myUserName, myOrg/mySubOrg)?",
				store: true,
			},
			{
				type: "input",
				name: "gitRepoName",
				message: "[Git] Repo name?",
				default: (currentAnswers: TAnswers) => currentAnswers.packageName,
			},
		];
	}

	private _ciQuestions(): Generator.Question<TAnswers>[] {
		return [
			{
				type: "list",
				name: "ciTool",
				message: "[CI] Set up boilerplate for?",
				choices: ["None", "GitLab CI"],
				default: "None",
			},
		];
	}

	configuring(): void {
		if (!this.answers) {
			throw new Error("No answers!");
		}

		for (let [answerKey, answer] of Object.entries(this.answers || {})) {
			if (answerKey === "gitRepoName") {
				answer = this.answers.gitRepoName || this.answers.packageName;
			} else if (answerKey === "packageFunction") {
				answer = this.answers.packageFunction || NodePackageFunction.DEPENDENCY;
			}
			this.config.set(answerKey, answer);
		}
		this.config.set(
			"fullPackageName",
			this.answers.packageScope && this.answers.packageScope !== "None"
				? `${this.answers.packageScope}/${this.answers.packageName}`
				: this.answers.packageName
		);
		this.config.set("nodeMajorVersion", parseInt((this.answers.nodeVersion || process.versions.node).split(".")[0]));
	}

	install(): void {
		this.spawnCommandSync("git", ["init"]);
	}

	end(): void {
		this.spawnCommandSync("git", ["add", "./.gitignore"]);
		this.spawnCommandSync("git", ["commit", "-m", "gitignore"]);
		this.spawnCommandSync("git", ["add", "."]);
		this.spawnCommandSync("git", ["commit", "-m", "init"]);
	}
}

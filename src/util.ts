import { TAllAnswers, TStandardGeneratorLifecycle } from "./types";
import path from "path";
import Generator from "yeoman-generator";
import assignIn from "lodash.assignin";

export const GENERATOR_NODE_MODULES_PATH = path.resolve(__dirname, "..", "node_modules");

// https://github.com/yeoman/generator/issues/1283; https://github.com/yeoman/generator/releases/tag/v5.0.0
// eslint-disable-next-line @typescript-eslint/no-var-requires
assignIn(Generator.prototype, require("yeoman-generator/lib/actions/install"));

// @ts-expect-error This is an abstract class
export abstract class ExtendedGenerator extends Generator implements TStandardGeneratorLifecycle {
	configGet<K extends keyof TAllAnswers>(key: K): TAllAnswers[K] {
		return this.config.get(key);
	}

	nodeModuleInstall({
		cwd,
		dev,
		pkgs,
		tool,
		toolOpts,
	}: {
		cwd?: string;
		tool: "npm" | "yarn";
		toolOpts?: Record<string, any>;
		pkgs: string | string[];
		dev?: boolean;
	}): void {
		const spawnOpts = cwd ? { cwd } : undefined;
		if (tool === "npm") {
			this.npmInstall(
				pkgs,
				{
					"prefer-offline": true,
					"no-audit": true,
					"save-dev": dev,
					...toolOpts,
				},
				spawnOpts
			);
		} else if (tool === "yarn") {
			this.yarnInstall(
				pkgs,
				{
					dev: dev,
					...toolOpts,
				},
				spawnOpts
			);
		}
	}
}

import { ExtendedGenerator } from "../util";

export default class extends ExtendedGenerator {
	answers?: any;

	writing(): void {
		this.fs.copy(this.templatePath("editorconfig.ejs"), this.destinationPath("./.editorconfig"));
	}
}

import { PackagePlatform, WebAppFunction } from "../types";
import { nodePkgs } from "../package-versions";
import { ExtendedGenerator } from "../util";
import * as path from "path";

export default class extends ExtendedGenerator {
	writing(): void {
		const { webAppClient, webAppFunction } = this.options;
		const packagePlatform = this.configGet("packagePlatform");
		const pkgJsonContext = {
			...this.config.getAll(),
			webAppClient,
			webAppFunction,
		};

		if (packagePlatform === PackagePlatform.WEB_APP) {
			this.fs.copyTpl(
				this.templatePath(webAppFunction === WebAppFunction.CLIENT ? "package.client.json.ejs" : "package.server.json.ejs"),
				this.destinationPath(webAppFunction === WebAppFunction.CLIENT ? path.join("client", "package.json") : "package.json"),
				pkgJsonContext
			);
		} else {
			this.fs.copyTpl(this.templatePath("npmignore.ejs"), this.destinationPath("./.npmignore"), this.config.getAll());
			this.fs.copyTpl(this.templatePath("package.json.ejs"), this.destinationPath("package.json"), pkgJsonContext);
		}
	}

	install(): void {
		if (this.options.webAppFunction === WebAppFunction.CLIENT) {
			this.nodeModuleInstall({
				cwd: this.destinationPath("client"),
				tool: "npm",
				pkgs: nodePkgs(["cross-env"]),
				dev: true,
			});
			return;
		}

		const devPkgs: string[] = ["chalk", "cross-env", "dotenv", "husky"];
		const packagePlatform = this.configGet("packagePlatform");
		if (packagePlatform === PackagePlatform.NODE) {
			devPkgs.push(`@types/node@${this.configGet("nodeMajorVersion") || "14"}`);
		}
		if (this.options.webAppFunction === WebAppFunction.SERVER) {
			devPkgs.push("concurrently");
		}
		this.nodeModuleInstall({
			tool: "npm",
			pkgs: nodePkgs(devPkgs),
			dev: true,
		});
	}
}

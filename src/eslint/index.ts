import { nodePkgs } from "../package-versions";
import { ExtendedGenerator } from "../util";
import { WebAppFunction } from "../types";

export default class extends ExtendedGenerator {
	answers?: any;

	writing(): void {
		const { webAppFunction } = this.options;
		this.fs.copy(
			this.templatePath("eslintignore.ejs"),
			this.destinationPath(webAppFunction === WebAppFunction.CLIENT ? "client/.eslintignore" : "./.eslintignore")
		);
		this.fs.copyTpl(
			this.templatePath("eslintrc.js.ejs"),
			this.destinationPath(webAppFunction === WebAppFunction.CLIENT ? "client/.eslintrc.js" : "./.eslintrc.js"),
			{
				webAppFunction: undefined,
				...this.options,
				packagePlatform: this.configGet("packagePlatform"),
			}
		);
	}

	install(): void {
		this.nodeModuleInstall({
			tool: "npm",
			pkgs: nodePkgs(["eslint", "eslint-config-prettier", "@typescript-eslint/eslint-plugin", "@typescript-eslint/parser"]),
			dev: true,
		});
	}
}

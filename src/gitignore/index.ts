import { ExtendedGenerator } from "../util";

export default class extends ExtendedGenerator {
	writing(): void {
		this.fs.copy(this.templatePath("gitignore.ejs"), this.destinationPath("./.gitignore"));
	}
}

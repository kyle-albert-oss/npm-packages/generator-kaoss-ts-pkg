import { nodePkgs } from "../package-versions";
import { ExtendedGenerator } from "../util";
import { WebAppFunction } from "../types";

export default class extends ExtendedGenerator {
	writing(): void {
		const { webAppFunction } = this.options;
		this.fs.copyTpl(
			this.templatePath("jest.config.js.ejs"),
			this.destinationPath(webAppFunction === WebAppFunction.CLIENT ? "client/jest.config.js" : "jest.config.js"),
			{
				webAppFunction: undefined,
				...this.options,
				packagePlatform: this.configGet("packagePlatform"),
			}
		);
	}

	install(): void {
		this.nodeModuleInstall({
			tool: "npm",
			pkgs: nodePkgs(["jest", "jest-extended", "@types/jest"]),
			dev: true,
		});
	}
}

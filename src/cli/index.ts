import { ExtendedGenerator } from "../util";

export default class extends ExtendedGenerator {
	writing(): void {
		this.fs.copy(this.templatePath("cli.ts.ejs"), this.destinationPath("src/cli.ts"));
	}
}

import { nodePkgs } from "../package-versions";
import { ExtendedGenerator } from "../util";

export default class extends ExtendedGenerator {
	writing(): void {
		this.fs.copy(this.templatePath("prettierignore.ejs"), this.destinationPath("./.prettierignore"));
		this.fs.copy(this.templatePath("prettierrc.ejs"), this.destinationPath("./.prettierrc.js"));
	}

	install(): void {
		this.nodeModuleInstall({
			tool: "npm",
			pkgs: nodePkgs(["prettier", "pretty-quick"]),
			dev: true,
		});
	}
}

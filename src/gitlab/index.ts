import { ExtendedGenerator } from "../util";

export default class extends ExtendedGenerator {
	writing(): void {
		this.fs.copy(this.templatePath("gitlab-ci.yml.ejs"), this.destinationPath("./.gitlab-ci.yml"));
	}
}

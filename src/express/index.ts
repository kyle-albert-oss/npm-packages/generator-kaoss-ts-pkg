import { nodePkgs } from "../package-versions";
import { ExtendedGenerator } from "../util";

export default class extends ExtendedGenerator {
	writing(): void {
		const context = {
			indexRootSubDir: "server",
			nodeVersion: this.configGet("nodeVersion"),
			...this.options,
		};
		this.fs.copyTpl(this.templatePath("nodemon.json.ejs"), this.destinationPath("nodemon.json"), context);
		this.fs.copyTpl(this.templatePath("index.ts.ejs"), this.destinationPath(`${context.indexRootSubDir}/index.ts`), context);
		// this.fs.copyTpl(this.templatePath("Dockerfile.ejs"), this.destinationPath("Dockerfile"), context); // TODO
	}

	install(): void {
		this.nodeModuleInstall({
			tool: "npm",
			pkgs: nodePkgs(["express"]),
		});
		this.nodeModuleInstall({
			tool: "npm",
			pkgs: nodePkgs(["@types/express", "nodemon"]),
			dev: true,
		});
	}
}

/**
 * Getting started: https://github.com/typescript-eslint/typescript-eslint/blob/master/docs/getting-started/linting/README.md
 * Rules: https://github.com/typescript-eslint/typescript-eslint/tree/master/packages/eslint-plugin
 */
module.exports = {
	root: true,
	plugins: ["@typescript-eslint"],
	parser: "@typescript-eslint/parser",
	parserOptions: {
		tsconfigRootDir: __dirname,
		project: ["./tsconfig.eslint.json"],
	},
	extends: [
		"eslint:recommended",
		"plugin:@typescript-eslint/recommended",
		"plugin:@typescript-eslint/recommended-requiring-type-checking",
		"prettier",
	],
	env: {
		commonjs: true,
		node: true,
	},
	rules: {
		"prefer-const": [
			"warn",
			{
				destructuring: "all",
			},
		],
		"require-await": "off",
		"@typescript-eslint/explicit-function-return-type": "off",
		"@typescript-eslint/no-inferrable-types": "off",
		"@typescript-eslint/no-unsafe-assignment": "off",
		"@typescript-eslint/no-unsafe-call": "off",
		"@typescript-eslint/no-unsafe-member-access": "off",
		"@typescript-eslint/no-unsafe-return": "off",
		"@typescript-eslint/require-await": "warn",
		"@typescript-eslint/restrict-template-expressions": "warn",
	},
};
